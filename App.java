import org.ho.yaml.Yaml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class App {

    private Scanner scanner;
    private String command;
    private ArrayList<Question> questions;

    public static void main(String[] args) throws IOException {
        App app = new App();
        app.init();
        app.start();
    }

    private void init() throws IOException {
        scanner = new Scanner(System.in);
        command = null;
        try {
            questions = (ArrayList<Question>) Yaml.load(new FileInputStream("src/Questions.yml"));
        } catch (ClassCastException e) {
            throw new IOException("Unable to read questions", e);
        } catch (org.ho.yaml.exception.YamlException ex) {
            questions = new ArrayList<>();
        }
    }

    private void start() throws FileNotFoundException {
        System.out.println("Добро пожаловать в систему для проведения тестирования!");
        printInfo();
        System.out.println("Введите команду:");
        while (!(command = scanner.nextLine()).equals("exit")) {
            switch (command) {
                case "add":
                    addNewQuestion();
                    break;
                case "start":
                    startTest();
                    break;
                case "help":
                    printInfo();
                    break;
                default:
                    System.out.println("Вы ввели неверную команду, повторите попытку.");
                    break;
            }
            System.out.println("Введите команду:");
        }
        finish();
    }

    private void addNewQuestion() {
        System.out.println("Введите в следующей строке текст вопроса:");
        String question = scanner.nextLine();
        System.out.println("Введите стоимость вопроса (натуральное число):");
        int cost = Integer.parseInt(scanner.nextLine());
        System.out.println("Введите количество вариантов ответа:");
        int numberOfQuestions = Integer.parseInt(scanner.nextLine());
        String[] answers = new String[numberOfQuestions];
        boolean[] isCorrect = new boolean[numberOfQuestions];
        for (int i = 0; i < numberOfQuestions; i++) {
            System.out.println("Введите " + (i + 1) + " вариант ответа:");
            answers[i] = scanner.nextLine();
            System.out.println("Если данный вариант ответа верный, введите +, иначе любой другой символ:");
            if ("+".equals(scanner.nextLine())) {
                isCorrect[i] = true;
            }
        }
        questions.add(new Question(question, cost, answers, isCorrect));
    }

    private void startTest() {
        System.out.println("Введите количество вопросов, на которое вы хотите ответить:");
        int numberOfQuestions = Integer.parseInt(scanner.nextLine());
        if (numberOfQuestions <= 0) {
            System.out.println("Вы ввели неположительное число. Повторите команду.");
        } else if (numberOfQuestions > questions.size()) {
            System.out.println("В системе пока нет такого количества вопросов. Добавьте новые вопросы.");
        } else {
            doTest(numberOfQuestions);
        }
    }

    private void doTest(int numberOfQuestions) {
        int result = 0;
        int maxSum = 0;
        int correctAnswers = 0;
        ArrayList<Question> mistakes = new ArrayList<>();
        Collections.shuffle(questions);
        long start = System.currentTimeMillis();
        for (int i = 0; i < numberOfQuestions; i++) {
            maxSum += questions.get(i).getCost();
            System.out.println(i + 1 + " вопрос:");
            System.out.print(questions.get(i).toString());
            System.out.println("Введите через пробел номера верных ответов:");
            String[] userAnswers = scanner.nextLine().split(" ");
            int numberOfMistakes = calculateNumberOfMistakes(userAnswers, i);
            int currentResult = calculateResultOfTest(numberOfMistakes, i);
            result += currentResult;
            if (currentResult != questions.get(i).getCost()) {
                mistakes.add(questions.get(i));
            } else {
                correctAnswers++;
            }
        }
        long finish = System.currentTimeMillis();
        long testTime = (finish - start) / 1000;
        System.out.println();
        System.out.println("Количество верных ответов " + correctAnswers + " / " + numberOfQuestions);
        System.out.println("Количество набранных баллов " + result + " / " + maxSum);
        System.out.println("Вы потратили " + testTime + " секунд на прохождение теста.");
        printListOfMistakes(mistakes);
    }

    private int calculateNumberOfMistakes(String[] userAnswers, int index) {
        int numberOfMistakes = 0;
        for (int j = 0; j < userAnswers.length; j++) {
            int userAnswer = Integer.parseInt(userAnswers[j]);
            if (userAnswer > 0 && userAnswer <= questions.get(index).getNumberOfAnswers()) {
                if (!questions.get(index).getIsCorrect()[userAnswer - 1]) {
                    numberOfMistakes++;
                }
            }
        }
        return numberOfMistakes;
    }

    private int calculateResultOfTest(int numberOfMistakes, int index) {
        if (numberOfMistakes < questions.get(index).getNumberOfCorrectAnswers()) {
            return questions.get(index).getCost()
                    * (questions.get(index).getNumberOfCorrectAnswers() - numberOfMistakes)
                    / questions.get(index).getNumberOfCorrectAnswers();
        }
        return 0;
    }

    private void printListOfMistakes(ArrayList<Question> mistakes) {
        if (mistakes.size() > 0) {
            System.out.println();
            System.out.println("Далее будут перечислены вопросы, в которых вы допустили ошибки, и верные ответы на них.");
            for (int i = 0; i < mistakes.size(); i++) {
                System.out.println();
                mistakes.get(i).printQuestion();
                mistakes.get(i).printCorrectAnswers();
            }
        }
        System.out.println();
    }

    private void printInfo() {
        System.out.println("Вам доступны следующие команды:");
        System.out.println("add   - добавить новый вопрос в систему;");
        System.out.println("start - начать тестирование;");
        System.out.println("help  - показать список доступных команд;");
        System.out.println("exit  - выйти из системы.");
    }

    private void finish() throws FileNotFoundException {
        Yaml.dump(questions, new FileOutputStream("src/Questions.yml"));
    }

}
