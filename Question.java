import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class Question implements Serializable {

    private String question; // текст вопроса
    private int cost; // стоимость вопроса
    private String[] answers; // массив вариантов ответов
    private boolean[] isCorrect; // массив индикаторов, показывающих верен ли соответствующий вариант ответа

    public Question() {
    }

    public Question(String question, int cost, String[] answers, boolean[] isCorrect) {
        this.question = question;
        this.cost = cost;
        this.answers = new String[answers.length];
        System.arraycopy(answers, 0, this.answers, 0, answers.length);
        this.isCorrect = new boolean[isCorrect.length];
        System.arraycopy(isCorrect, 0, this.isCorrect, 0, isCorrect.length);
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setAnswers(String[] answers) {
        this.answers = new String[answers.length];
        System.arraycopy(answers, 0, this.answers, 0, answers.length);
    }

    public void setIsCorrect(boolean[] isCorrect) {
        this.isCorrect = new boolean[isCorrect.length];
        System.arraycopy(isCorrect, 0, this.isCorrect, 0, isCorrect.length);
    }

    public String getQuestion() {
        return question;
    }

    public int getCost() {
        return cost;
    }

    public String[] getAnswers() {
        return answers;
    }

    public boolean[] getIsCorrect() {
        return isCorrect;
    }

    public int getNumberOfAnswers() {
        return answers.length;
    }

    public int getNumberOfCorrectAnswers() {
        int numberOfCorrectAnswers = 0;
        for (int i = 0; i < isCorrect.length; i++) {
            if (isCorrect[i]) {
                numberOfCorrectAnswers++;
            }
        }
        return numberOfCorrectAnswers;
    }

    public void printQuestion() {
        System.out.println(question);
    }

    public void printCorrectAnswers() {
        for (int i = 0; i < isCorrect.length; i++) {
            if (isCorrect[i]) {
                System.out.println(answers[i]);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(question);
        sb.append("\n");
        sb.append("Стоимость вопроса: ");
        sb.append(cost);
        sb.append("\n");
        sb.append("Варианты ответов:\n");
        for (int i = 0; i < answers.length; i++) {
            sb.append(i + 1);
            sb.append(") ");
            sb.append(answers[i]);
            sb.append("\n");
        }
        return sb.toString();
    }

}
